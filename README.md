# FLIGHT: Packard Interactive Light Sculpture Project

This repository stores all of the files, notes, and technical documents
for FLIGHT, the interactive light sculpture for the Packard building at
Stanford Unviersity.  Stanford students
taking EE185, EE285, and CS241, under guidance from course instructors,
engineer an art piece designed by Charles Gadeken. This engineering involves
designing, building, and testing the mechanical, electrical, and software
components of the pieces.

The directory structure:
  - autumn: materials from the Autumn offering of EE185.
  - winter: materials from the Winter offering of EE185/EE285/CS241.
  - spring: materials from the Spring offering of EE185/EE285/CS241.
  - FRACTAL\_FLYER: all technical information on Fractal Flyers, the moving, illuminated objects that form FLIGHT. 76 Fractal Flyers are hung in Packard to form the complete FLIGHT installation.
  - software: the software to control and visualize FLIGHT, including firmware, UI, and simulation.
  - packard: architectural drawings and models of Packard, including its stairwell
