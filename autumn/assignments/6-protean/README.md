# Assignment 6:  Primitive Form

**Due: Monday, November 11, 2019 at 10:00 AM**

We're now ready to make our first full, working prototype. There
is one item due: two copies of the prototype. If you can only make
one, that's OK, but it would be better to have two (or at least parts
of 2). 

You'll work in subgroups on different aspects of the design and bring it 
all together as a group.
The EE faculty weekly lunch on Monday 11/11 will come to Packard 001 to 
see the prototype, and we will show it at the EE125 celebration on 
Wednesday, 11/13. We'll talk in class on Monday about how everyone can
participate.

Let's wow Charlie when he's back on Monday!

## Shape and Design

We agreed to pursue design 1's shape while using the material choices in design 3. There
was also discussion about using something like the hinge approach in design 2. 

The wings of the shape should have the geometry of the small shape 1 on the center table
(with etched wings). Charlie would like to pursue a shape where the profile from above
is the profile of the building. This wing shape reaches out much less from the body, so
will require less torque than wider wings. You are welcome to explore other wings designs
(e.g., those on the shape 1 shown in class) -- we'll go with what looks
best. But please include the shapes that Charlie suggested.

If something better appears, feel free to pursue it, but our current plan is to
clear acrylic wings with dichroic film, while the body is a combination
of black acrylic and frosted acrylic with dichroic film. 

## Subgroups and Assignments

We are breaking the team into five subgroups:

  1. Illumination (*Michal, Tim*, Claire, David): Get flexible PCB LED strips
     from Professor Horowitz. Determine how to cleanly attach LEDs to the
     wings and interior and the wiring to the compute board. Take wing shape
     from motion group. Add etching to design. Apply dichroic film, cut wing
     shapes, attach and wire LEDs.
  2. Motion (*Will, Claire*, Lee): Decide on mechanism for wing motion.
     Coordinate with computing group on what motor control will look like
     (e.g., PWM or other approach).
     Work with installation group to determine how mechanism affects
     the body and how motor will be attached. Define wing shape and give
     to illumination group to prepare for use.
  3. Computing (*Mihir, Sean*, Michal, Tim): Write program to control wings so 
     they slowly move at a reasonable range of motion within what the
     shape and motion assembly allow. Coordinate with motion group to
     determine these parameters. Include LED control and animations in
     program. Coordinate with illumination group on how LEDs are wired
     and so how software will address them for different visual effects.
  4. Power (*David, Kelly*, Sean): Determine power budget based on compute
     board, LEDs, and motors. Choose a power supply that can provide the
     required power. Work with installation group on how power will be brought
     into the body and distributed to the different components while being
     safely insulated. For the demo, we can have wall power.
  5. Installation (*Andrea, Lee*, Will): Design and build body. Explore fastener
     approaches. Decide on flush body faces vs. gaps. Work with
     motion group to attach motion assembly and mechanisms to a firm structure
     on body. Work with power and illumination groups on how power will come in
     and be brought to LEDs on wings. Build a stand for the piece to be on,
     coordinate with power group on bringing power in from the stand.

There's a lot of overlap and touch points between the groups. It's very important
that you communicate to figure out exactly where the boundaries are, both so that
you don't come up with mutually incompatible approaches as well as so nothing
falls between the cracks. If someone sends a Slack channel to -staff, we can
email it out to the entire class (or you can email the class yourself).

If you need to purchase some small supplies (fasteners, etc.), please
keep all of your receipts. You have per-person $50 budget. If you need more expensive
materials, please email the staff list. Finally, if you want materials for the 
exterior that have some lead time, decide on this quickly so you'll have them.

Finally, if you're blocking on something, please feel free to reach out to
the -staff list and we will help you however we can.

## Handing In

Bring your completed shape to class on Monday, November 11th!

 


