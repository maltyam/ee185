# Project: Mechanism and Top Plate Production 

*Instructor Lead: Philip Levis*

**Due: Tuesday, Feb 18th, 2020**

The goals and deliverables for this project are to:
  1. Finalize exact mechanism placement and holes needed in top plate,
  1. Determine top plate material and thickness,
  1. Decide and document the parts list for the mechanism,
  1. Build 2 full prototypes of top plate + mechanism,
  1. Start a pair of wings moving for continuous operation indefinitely,
  1. Write up a report with the complete bill of materials, proposed sources,
     and assembly instructions, and
  1. When report is approved, order parts

## Finalize placement and top plate effects

Decide exactly where the mechanism will be placed and update the top plate
layout accordingly. As you do this, coordinate with the hinge, body, 
diffusion and wiring groups. 

 - With the hinge group, coordinate on where the top plate
hinge leaves will be placed, their hole widths, spacings, and locations.
 - With the body group, coordinate on where the keyhole connectors are placed,
considering the flange and clearance of objects within the body. 
 - With the wiring group, coordinate on the placement of the tail board
   and RJ45 jack.
 - With the diffusion group, coordinate on the LED plate spacing and location.

The top plate will not be complete yet -- there are several other things
which will be added to it. But you will finalize exactly where the mechanism
goes and how it mounts.

## Determine top plate material and thickness

The current plan is to use stainless steel for the top plate, for its
strength-to-weight ratios and resistance to corrosion. Decide on what
alloy to use and the plate thickness. Double-check that the alloy is
compatible with the press-fit connectors for the flange. Decide the plate 
thickness by analyzing and/or simulating the forces in play. Determine the 
absolute minimum thickness and your recommended thickness. Commit all
source files (writeups of analysis, simulation files, etc.) to the 
repository.

## Decide and document the parts list

Finalize the exact parts list for the mechanism, down to the nut, bolt, and
wire. This should be a complete list of everything needed for the mechanism,
including the shaft but not including the hinge leaves. 
The list should include
the top plate material and thickness with references to the documents
used to reach this result. 

## Build 2 full prototypes

Using the part list, source materials for at least 2 complete Fractal Flyers
(so, 4 mechanisms). Source an additional 2 top plates, one slightly thicker
and one slightly thinner than the minimum thickness you calculated.
Build them. Take notes as you build them on the best
build process and any gotchas, such as order of operations and tightness
of bolts. 

## Start wings moving

Take one of your prototypes and get a pair of sample wings moving continuously.
Leave them moving indefinitely. Do not unplug them, change them, etc. The
goal is to start a longevity test and run it as long as possible.

## Write BOM and proposed sourcing

Take your parts list and write it up a complete bill of materials for the
mechanism for 100 Fractal Flyers, updated
with any errors found in building the prototypes.  Include URLs and exact
part numbers for any non-standard parts. Document proposed sourcing
mechanisms. Commit this as a simple text file in the repository in a
`bom/` directory.

## Send your BOM and assembly instructions for review

Notify course staff when your BOM is ready for review. Take your
assembly instructions and write them up as a Markdown or PDF, and
commit it to the repository. Send a copy of the instructions to the
class list for review and comments.

## Purchase Materials

When your BOM has been approved by the instructors, work with
Steve to order the parts.

