/* 
 * Defines the main pipeline for data transmission 
 * from the engine to the core simulator process. 
 * 
 * Creates named pipes into each fractal flier's
 * filesystem, reads in python configuration data
 * spawns an interpreter and seperate process for 
 * each flier, which write to a socket connected
 * to the core simulator process.
 *
 * To compile: g++ -Wall --std=c++11 -I PEGTL/ -O2 data_processor.cc -c -o data_processor.o 
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <vector>
#include <sys/inotify.h>
#include <string>
#include <fcntl.h>

using namespace std; 

#define NUM_FF 2 //TODO(SD): Change to 76 when appropriate synchronization exists. 
#define BUF_LENGTH 1000 

/* https://stackoverflow.com/questions/23498654/read-from-a-named-pipe TODO(SD): For testing named pipe. 
 * and https://docs.python.org/2/extending/embedding.html for embedding python interpreter. */ 

int Fd[NUM_FF]; 
//int Pid[NUM_FF]; 

struct flier {
    //flier() {} //may need separate constructor here. 
    int fd; //Associated w/ Fd of this flier's inotify instance queue.  
    int wd; //Associated with watch descriptor for inotify instance. 
    int np; //File descriptor associated with flier's named pipe. 
    int pid; //Pid associated with the flier's process. 
};  

static vector<flier> fliers(NUM_FF); 

//TODO (AM): Lots of fun systems stuff to handle inotify and assorted signals, + reap 
//processes at the end of execution. 

void spawn_process (/*char * init_directory,*/ int flier_num, struct flier &new_flier) 
{ 
    new_flier.pid = fork(); 
    char buf[BUF_LENGTH]; 
    while (new_flier.pid == 0) { //TODO(SD): Only read upon receiving an inotify. 
        if (read(new_flier.np, buf, BUF_LENGTH)) { 
            printf("Read in: %d bytes, %s\n", BUF_LENGTH, buf); 
            //TODO (SD): Use python interpreter + socket to write to core simulator process. 
        } 
        
    } 

} 

/* Sets up a specific flier struct with its own named pipe. */ 
void create_fifo (/*char * init_directory,*/ int flier_num, struct flier &new_flier)
{ 
    //First, set up inotify instance for change notification, to-do, deal with signals later. 
    
    //TODO (SD): Lots of error checking. 
    new_flier.fd = inotify_init(); 
    const char * directory = ("Directory" + to_string(flier_num)).c_str();  
    int dic = mkdir(directory, S_IRGRP); 
    new_flier.wd = inotify_add_watch(new_flier.fd, directory, IN_MODIFY); 
    
    //TODO (SD): Define persmissions mask for np, now just random flags. 
    const char * fifo_dict = ("Directory" + to_string(flier_num) + "/Serial_port").c_str(); 
    int fifo_ret = mkfifo(fifo_dict, S_IWUSR); //TODO(SD): Same flow as above, make into separate function.  
    new_flier.np = open(fifo_dict, O_RDWR);    
}     

struct flier spawn_new_flier (/*char * init_directory,*/ int flier_num) { 
    struct flier new_flier; //TODO(SD): Need to heap allocate?  
    create_fifo (/*init_directory,*/ flier_num, new_flier); 
    spawn_process (/*init_directory,*/ flier_num, new_flier); 
    return new_flier; 
} 

/* Takes init directory as char * and passes onto simulator. */ 

void create_data_pipeline(/*char * init_directory*/)
{ 
    for (int i =0 ; i < NUM_FF; i++) { 
        fliers.push_back(spawn_new_flier(/*init_directory,*/ i));
    } 
} 

/* 
 * Main takes in an initialization directory of 
 * the basic python framework from the caller. 
 */ 

//TODO(SD): Eventually define an init directory to pass into the process. 
int main(/*int argc, char *argv[]*/)
{
	create_data_pipeline(/*argv[0]*/); 
	return 0;
}
