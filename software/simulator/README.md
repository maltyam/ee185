# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores source code for a graphical FLIGHT simulator. The simulator
appears exactly as the art installation does, like 76 CircuitPython instances. You
can program and visualize how they look using a high-quality 3D visualization.

#PYTHON API FORMAT 

## Python API

For a Python API, I’d suggest these three functions:

wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b,), (r, g, b,))
body_leds((r, g, b)
wing_angle(LEFT|RIGHT, angle)

Where r g and b are 0-255 and angle is -90.0 - 90.0.

The assumption is the LED changes are near-instantaneous. The angle change, 
however, may take time. The FF is smart enough to know its current wing position
and velocity and just try to move to the most recent angle command. So, suppose 
the left wing is at 0 and you call wing_angle(LEFT, -20),the left wing will 
start to lower to -20. Suppose, when it is at -10, you call wing_angle(LEFT, -30).
It will keep on lowering until it reaches -30. If, when it is at -25, you call 
wing_angle(LEFT, 10), it will slow and stop, then start moving to 10. 
This will all occur on the firmware.