import java.util.ArrayList;
import heronarts.lx.LX;
import heronarts.lx.pattern.LXPattern;
import heronarts.lx.clip.LXClip;
import heronarts.lx.mixer.LXBus;

abstract class Engine {

  static final int NUM_CHANNELS = 8;
  static final int NUM_AUTOMATION = 4;

  // General engine variables
  final String projectPath;
  final IO io;
  final FlyerConfig[] flyerConfig;
  final LX lx;
  final Flight model;
  EngineController engineController;
  Generator generator;

  // Automation variables
  final FFAutomationClip[] automation = new FFAutomationClip[Engine.NUM_AUTOMATION];

//  ArrayList<TSPattern> patterns;

  /** CONSTRUCTOR **/
  Engine(String projectPath) {
    this.projectPath = projectPath;
    io = new IO(projectPath);

    flyerConfig = io.loadConfigFile(Config.FLYER_CONFIG_FILE);
    model = new Model(flyerConfig).getFlightModel();

    lx = createLX();
    postCreateLX();

    engineController = new EngineController(lx);

    /** All engine configuration **/
    configureChannels();
    configureAutomation();

    /** For testing **/
    lx.engine.addLoopTask(new FadeTest(lx));

//    String testPlaylistFilename = "data/playlist.json";
//    JsonArray testPlaylist = io.loadSetFile(testPlaylistFilename);
//
//    testPlaylist.get(3).getAsJsonObject().addProperty("new param", "heyaaa");
//    io.saveSetFile(testPlaylist, testPlaylistFilename);

  }

  /***** PUBLIC FUNCTIONS *****/

  /* Defines the LX object to create, Java LX or P3LX */
  abstract LX createLX();

  /* Steps that have to be done after creating LX, overridden by Engine subclass */
  public void postCreateLX() {}

  /* Start the LX engine */
  public void start() {
    lx.engine.start();
  }

  /***** PRIVATE FUNCTIONS *****/

  /* Adds patterns to the pattern list passed as parameter */
  private void addPatterns(ArrayList<LXPattern> patterns) {
    // Add patterns here.
    // The order here is the order it shows up in the patterns list
    // patterns.add(new SolidColor(lx));
    // patterns.add(new ClusterLineTest(lx));
    // patterns.add(new OrderTest(lx));
  }

  /* Returns the pattern list as an array */
  private LXPattern[] getPatternArrayForChannels() {
    ArrayList<LXPattern> patterns = new ArrayList<LXPattern>();
    addPatterns(patterns);
    return patterns.toArray(new LXPattern[patterns.size()]);
  }

  /* Configures channels */
  private void configureChannels() {
    for (int i = 0; i < Engine.NUM_CHANNELS; ++i) {
      //LXChannel channel = new LXChannel(lx, i, getPatternArrayForChannels());
      //TODO: Set up the channels
    }
  }

  /* configureGeneratorOutput */
  void configureGeneratorOutput() {
    try {
      generator = new Generator(lx, model, null, null);
      lx.addOutput(generator);
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  /* Configures the automation */
  private void configureAutomation() {

  }

}

/** EngineController Class **/
class EngineController {
  LX lx;

  EngineController(LX lx) {
    this.lx = lx;
  }

}

/** FFAutomationClip Class **/
class FFAutomationClip extends LXClip {

  FFAutomationClip(LX lx, LXBus bus, int index) {
    super(lx, bus, index);
  }

}
