final class Config {

  static final boolean enableAPC40 = true;

  static String[] AUTOMATION_FILES = {"data/Burning Man Playlist.json"};

  static final String FLYER_CONFIG_FILE = "data/flyers.json";

}
