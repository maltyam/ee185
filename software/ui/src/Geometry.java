import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXPoint;

interface GeometryConstants {
  final static int INCHES = 1;
  final static int FEET = 12 * INCHES;

  /**
   * room dimensions
   */
  final static float X_ROOM = 40 * FEET;
  final static float Y_ROOM = 40 * FEET;
  final static float Z_ROOM = 40 * FEET;

  /**
   * num_x_per_y constants
   */
  final static int NUM_WINGS_PER_FLYER = 2;
  final static int NUM_LIGHT_POINTS_PER_WING = 3;
  final static int NUM_LIGHT_POINTS_PER_BODY = 1;
  final static int NUM_LIGHT_POINTS_PER_FLYER = NUM_WINGS_PER_FLYER * NUM_LIGHT_POINTS_PER_WING + NUM_LIGHT_POINTS_PER_BODY;

  /**
   * indexing scheme constants
   */
  final static int NUM_FLYERS = 76;
  final static int NUM_WINGS = NUM_FLYERS * NUM_WINGS_PER_FLYER;
  final static int NUM_WING_LIGHT_POINTS = NUM_WINGS * NUM_LIGHT_POINTS_PER_WING;
  final static int NUM_BODY_LIGHT_POINTS = NUM_FLYERS;
  final static int NUM_TOTAL_LIGHT_POINTS = NUM_WING_LIGHT_POINTS + NUM_BODY_LIGHT_POINTS;


  /**
   * Pattern constants
   */
  final static String COLOR = "FF99FF";
  final static int BRIGHTNESS = 100;
  final static String LIGHT_PATTERN = "lightning";
  final static String MOVEMENT_PATTERN = "gentle_flap";




  /**
   * Body LED dimensions
   */
  // TODO (achen) am i interpreting this right?
  public final static float LENGTH_BODY_CROSS = 22 * INCHES;
  public final static float WIDTH_BODY_CROSS = (float) (3.8125 * INCHES);
  public final static float DEPTH_BODY_CROSS = (float) (.394 * INCHES);

  /**
   * Wing LED dimensions
   */
  final static float CORNER_DISTANCE = (float) (7.06 * INCHES);
  final static float TOP_EDGE_DISTANCE = (float) (1.18 * INCHES);
  final static float SIDE_EDGE_DISTANCE = (float) (18.04 * INCHES);
  final static float INNER_EDGE_DISTANCE = (float) (20.69 * INCHES);
  final static float WING_THICKNESS_MM = 8;
  List<LXPoint> wingPoints = new ArrayList<LXPoint>(Arrays.asList(
    new LXPoint(3.480, 0.152, 0),
    new LXPoint(0.005, 6.297, 0),
    new LXPoint(0.148, 6.542, 0),
    new LXPoint(20.841, 6.539, 0),
    new LXPoint(20.919, 6.179, 0),
    new LXPoint(3.974, 0.005, 0)
//    3.480 0.152
//    0.005 6.297
//    0.148 6.542
//    20.841 6.539
//    20.919 6.179
//    3.974 0.005
  ));

  LXModel wing = new LXModel(wingPoints);

  List<LXPoint> wingLightPoints = new ArrayList<LXPoint>(Arrays.asList(
    new LXPoint(0.0765, 6.4195, 0), // head
    new LXPoint(3.727, 0.0785, 0), // side
    new LXPoint(20.88, 6.359, 0) // tail
  ));

  LXModel wingLights = new LXModel(wingLightPoints);

}