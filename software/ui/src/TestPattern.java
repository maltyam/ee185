import heronarts.lx.LX;
import heronarts.lx.modulator.SinLFO;
import heronarts.lx.pattern.LXPattern;
import heronarts.lx.parameter.BoundedParameter;


class FadeTest extends LXPattern {

    final BoundedParameter speed = new BoundedParameter("SPEE", 11000, 100000, 1000);
    final BoundedParameter smoothness = new BoundedParameter("SMOO", 100, 1, 100);

    final BoundedParameter low = new BoundedParameter("LOW", 0, 0, 360);
    final BoundedParameter high = new BoundedParameter("HIGH", 360, 0, 360);

    final SinLFO color = new SinLFO(0, 360, speed);

    protected final Flight model;

    FadeTest(LX lx) {
        super(lx);
        model = (Flight)lx.getModel();
        addParameter(speed);
        addParameter(smoothness);
        addParameter(low);
        addParameter(high);
        addModulator(color).start();
    }

    public void run(double deltaMs) {
        color.setRange(low.getValuef(), high.getValuef());
        for (LightSamplePoint lightPoint : model.getAllLights()) {
            lightPoint.setColor(String.valueOf(lx.hsb(
                    (int)((int)color.getValuef() * smoothness.getValuef() / 100) * 100 / smoothness.getValuef(),
                    100,
                    100)
            ));
        }
    }
}