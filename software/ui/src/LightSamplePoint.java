interface LightSamplePoint extends GeometryConstants {
  enum LightCorners {
    HEAD(0, 24), // half short (.09) + half long (.27) + .03 = .39 , 23.4 LEDS
    SIDE(1, 21), // half short (.09) + half medium (.23) + .03 = .35, 21 LEDS
    TAIL(2, 31), // half medium (.23) + half long (.26) + .03 = .52,  31.2 LEDs
    // TODO (achen) figure out how many LEDS in each part of body
    BODY(6, 99);
    //  The long edge of a wing is 20.69 inches. (.53m)
    //  The medium edge is 18.04 edges.  (.46m)
    //  The short edge is 7.06 inches.   (.18m)
    //  corners are .03 m each
    public final int position;
    public final int numPixels;
    private LightCorners(int position, int numPixels) {
      this.position = position;
      this.numPixels = numPixels;
    }
  }

  Integer getWingIndex();
  int getBodyIndex();
  LightCorners getLocation();

  /**
   * Index of this lightPoint in color buffer, colors[lightPoint.getIndex()]
   */
  default int getIndex() {
    Integer wingIndex = getWingIndex();
    int bodyIndex = getBodyIndex();
    if (wingIndex != null) {
      if (wingIndex % 2 == 0) { // this is a left wing
        return bodyIndex * NUM_LIGHT_POINTS_PER_FLYER + getLocation().position;
      }
      return bodyIndex * NUM_LIGHT_POINTS_PER_FLYER + NUM_LIGHT_POINTS_PER_WING + getLocation().position;
    }
    return bodyIndex * NUM_LIGHT_POINTS_PER_FLYER + getLocation().position;
  }

  void setColor(String color);
  String getColor();
  void setBrightness(int brightness);
}