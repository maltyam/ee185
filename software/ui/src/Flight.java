import java.util.List;

interface Flight extends GeometryConstants {
  /**
   * for movement pattern
   */
  List<Wing> getLeftWings();

  List<Wing> getRightWings();

  List<Wing> getAllWings();

  /**
   * for light pattern
   */
  List<LightSamplePoint> getLightsOfLeftWings();

  List<LightSamplePoint> getLightsOfRightWings();

  List<LightSamplePoint> getLightsOfAllWings();

  List<LightSamplePoint> getHeadLightOfLeftWings();

  List<LightSamplePoint> getHeadLightOfRightWings();

  List<LightSamplePoint> getHeadLightOfAllWings();

  List<LightSamplePoint> getSideLightOfLeftWings();

  List<LightSamplePoint> getSideLightOfRightWings();

  List<LightSamplePoint> getSideLightOfAllWings();

  List<LightSamplePoint> getTailLightOfLeftWings();

  List<LightSamplePoint> getTailLightOfRightWings();

  List<LightSamplePoint> getTailLightOfAllWings();

  List<LightSamplePoint> getLightsOfAllBodies();

  List<LightSamplePoint> getAllLights();

  FlyerConfig[] getFlyerConfigs();

  Flyer getFlyer(int i);

  Flyer[] getFlyers();

/**
 * This defines the positions of the flyers, which are
 * index,
 * x (left to right), y (front to back), z (bottom to top),
 * rotation in degrees, and tilt (3 options)
 */


}