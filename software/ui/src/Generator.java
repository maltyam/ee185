//package src;

// Generate commands for one flyer
// Need access to right and left rgb, body rgb and right and left angle values
// Take flyer instance as parameter

import java.util.ArrayList;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import heronarts.lx.LX;
import heronarts.lx.output.LXOutput;
import java.awt.Color;



/*
 * Generates python command for fractal flyer
 * 	TODO change colour representation in model from string to value
 */
public class Generator extends LXOutput implements GeometryConstants {
  final Flight flight;
  final Flyer[] flyers;
  final DataInput[] in;
  final DataOutput[] out;

  static final String LEFT_WING_POS = "wing_angle(LEFT, %s)\n";
  static final String RIGHT_WING_POS = "wing_angle(RIGHT, %s)\n";
  static final String LEFT_WING_LEDs = "wing_leds(LEFT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))\n";
  static final String RIGHT_WING_LEDs = "wing_leds(RIGHT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))\n";
  static final String BODY_LED = "body_leds((%d, %d, %d))";


  Generator(LX lx, Flight flight, DataInput[] inSockets, DataOutput[] outSockets) {
    super(lx);
    this.flight = flight;
    this.flyers = this.flight.getFlyers();
    this.in = inSockets;
    this.out = outSockets;
  }

  private Color[] getRGBColors(LightSamplePoint[] points) {
	  Color[] ledColors = new Color[points.length];
	  for (int i = 0; i < points.length; i++) {
		  ledColors[i] = new Color(Integer.parseInt(points[i].getColor()));
	  }
	  return ledColors;
  }

  /*
   * ## Python API

		For a Python API, I’d suggest these three functions:

		wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b,), (r, g, b,))
		body_leds((r, g, b)
		wing_angle(LEFT|RIGHT, angle)
   */

  /*
   * TODO handle the case where only one parameter has changed
   * Perhaps that should be handled by engine
   */

  private ArrayList<String> generateCommands(Flyer flyer) {
    /*
     * String s = "hello %s!";
     * s = String.format(s, "world");
     * assertEquals(s, "hello world!");
     */
	Wing[] wings = flyer.getWings();
	Wing lWing = flyer.getLeftWing();
	Wing rWing = flyer.getRightWing();

	int leftWingPos = lWing.getSkew();
	int rightWingPos = rWing.getSkew();

	LightSamplePoint bodyLightPoint = flyer.getBodyLightPoint();
	LightSamplePoint[] leftLEDs = lWing.getLightPoints();
	LightSamplePoint[] rightLEDs = rWing.getLightPoints();

    ArrayList<String> commands = new ArrayList<String>();

    Color[] leftLights = getRGBColors(leftLEDs);
    Color[] rightLights = getRGBColors(rightLEDs);
    Color bodyLED = new Color(Integer.parseInt(bodyLightPoint.getColor()));


    if (leftWingPos >= 0) {
      commands.add(String.format(LEFT_WING_POS, leftWingPos));
    }

    if (rightWingPos >= 0) {
      commands.add(String.format(RIGHT_WING_POS, rightWingPos));
    }

    commands.add(String.format(BODY_LED,
    		bodyLED.getRed(), bodyLED.getGreen(), bodyLED.getBlue()));

    commands.add(String.format(LEFT_WING_LEDs,
    		leftLights[0].getRed(), leftLights[0].getGreen(), leftLights[0].getBlue(),
    		leftLights[1].getRed(), leftLights[1].getGreen(), leftLights[1].getBlue(),
    		leftLights[2].getRed(), leftLights[2].getGreen(), leftLights[2].getBlue())
    		);

    commands.add(String.format(LEFT_WING_LEDs,
    		rightLights[0].getRed(), rightLights[0].getGreen(), rightLights[0].getBlue(),
    		rightLights[1].getRed(), rightLights[1].getGreen(), rightLights[1].getBlue(),
    		rightLights[2].getRed(), rightLights[2].getGreen(), rightLights[2].getBlue())
    		);

    return commands;
  }

  @Override
  protected void onSend(int[] colors, double brightness) {
    super.onSend(colors, brightness);

    for (int i=0; i<flyers.length; i++) {
        ArrayList<String> commands = generateCommands(flyers[i]);

        for (String command: commands) {
          try {
            out[i].writeBytes(command);
          } catch (IOException e) {
            System.out.println("failed to write command!");
          }
        }

    }
  }

  @Override
  protected void onSend(int[] arg0, byte[] arg1) {
    throw new UnsupportedOperationException("LXDatagramOutput does not implement onSend by glut");
  }

}
