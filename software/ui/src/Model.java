import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.ArrayList;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXPoint;


// TODO (achen) add LightSamplePoints as LXPoints
// more pattern support

class Model {
  static Flight flight;

  public Model(FlyerConfig[] flyerConfig) {
    flight = new FlightModel(flyerConfig);
  }

  Flight getFlightModel() {
    return flight;
  }
}

class FlightModel extends LXModel implements Flight {
  /**
   * Flyers in the model
   */
  protected Flyer[] flyers = new Flyer[NUM_FLYERS];

  /**d
   * Wings in the model
   */
  protected Wing[] wings = new Wing[NUM_WINGS];

  /**
   * lightPoints in the model
   */
  protected LightSamplePoint[] lightPoints = new LightSamplePoint[NUM_TOTAL_LIGHT_POINTS];

  FlightModel(Flyer[] flyers) {
    for (Flyer flyer : flyers) {
      for (Wing wing : flyer.getWings()) {
        wings[wing.getIndex()] = wing;

        for (LightSamplePoint lightPoint : wing.getLightPoints()) {
          lightPoints[lightPoint.getIndex()] = lightPoint;
        }
      }

      LightSamplePoint bodyLightPoint = flyer.getBodyLightPoint();
      lightPoints[bodyLightPoint.getIndex()] = bodyLightPoint;
    }
  }

  FlightModel(FlyerConfig[] flyerConfigs) {
    for (int i = 0; i < NUM_FLYERS; i++) {
      FlyerConfig config = flyerConfigs[i];
      FlyerModel flyer = new FlyerModel(config.index, config.x, config.y, config.z, config.rotation, config.tilt);

      for (Wing wing : flyer.getWings()) {
        wings[wing.getIndex()] = wing;

        for (LightSamplePoint lightPoint : wing.getLightPoints()) {
          lightPoints[lightPoint.getIndex()] = lightPoint;
        }
      }

      LightSamplePoint bodyLightPoint = flyer.getBodyLightPoint();
      lightPoints[bodyLightPoint.getIndex()] = bodyLightPoint;

      flyers[i] = flyer;
    }
  }

  @Override
  public List<Wing> getLeftWings() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % 2 == 0)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<Wing> getRightWings() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % 2 == 1)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<Wing> getAllWings() {
    return IntStream
      .range(0, wings.length)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getLightsOfLeftWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> (i % 7 == 0 || i % 7 == 1 || i % 7 == 2))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getLightsOfRightWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> (i % 7 == 3 || i % 7 == 4 || i % 7 == 5))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getLightsOfAllWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 != 6)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }


  @Override
  public List<LightSamplePoint> getHeadLightOfLeftWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 0)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getHeadLightOfRightWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 3)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getHeadLightOfAllWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> (i % 7 == 0 || i % 7 == 3))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getSideLightOfLeftWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 1)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getSideLightOfRightWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 4)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getSideLightOfAllWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> (i % 7 == 1 || i % 7 == 4))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getTailLightOfLeftWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 2)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getTailLightOfRightWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 5)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getTailLightOfAllWings() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> (i % 7 == 2 || i % 7 == 5))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getLightsOfAllBodies() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(i -> i % 7 == 6)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePoint> getAllLights() {
    return Arrays.asList(lightPoints);
  }

  @Override
  public FlyerConfig[] getFlyerConfigs() {
    List<FlyerConfig> flyerConfigs = new ArrayList<FlyerConfig>();
    for (Flyer flyer: flyers) {
      flyerConfigs.add(flyer.getConfig());
    }
    return flyerConfigs.toArray(new FlyerConfig[0]);
  }

  @Override
  public Flyer getFlyer(int i) {
    return flyers[i];
  }

  @Override
  public Flyer[] getFlyers() {
    return flyers;
  }
}

class FlyerModel extends LXModel implements Flyer {
  /**
   * index of the flyer
   */
  protected int index;

  /**
   * x-position of center of the flyer
   */
  protected float x;

  /**
   * y-position of center of the flyer
   */
  protected float y;

  /**
   * z-position of center of the flyer
   */
  protected float z;

  /**
   * rotation of the flyer in the xy plane, in degrees, 0 - 360 clockwise
   */
  protected float rotation;

  /**
   * degree of flyer tilt, 0 - 180, with 0 being head down, 180 being head up
   */
  protected float tilt;

  protected Wing[] wings = new Wing[NUM_WINGS_PER_FLYER];

  protected LightSamplePoint bodyLightPoint;

  FlyerModel(int index, float x, float y, float z, float rotation, float tilt) {
    this.index = index;
    this.x = x;
    this.y = y;
    this.z = z;
    this.rotation = rotation;
    this.tilt = tilt;

    this.wings[0] = new WingModel(this.index, false);
    this.wings[1] = new WingModel(this.index, true);

    this.bodyLightPoint = new LightSamplePointModel(LightSamplePoint.LightCorners.BODY, null, this.index, COLOR, BRIGHTNESS);
  }

  @Override
  public int getIndex() {
    return this.index;
  }

  @Override
  public float getX() {
    return this.x;
  }

  @Override
  public float getY() {
    return this.y;
  }

  @Override
  public float getZ() {
    return this.z;
  }

  @Override
  public float getRotation() {
    return this.rotation;
  }

  @Override
  public float getTilt() {
    return this.tilt;
  }

  @Override
  public Wing[] getWings() {
    return this.wings;
  }

  @Override
  public Wing getLeftWing() {
    return this.wings[0];
  }

  @Override
  public Wing getRightWing() {
    return this.wings[1];
  }

  @Override
  public LightSamplePoint getBodyLightPoint() {
    return this.bodyLightPoint;
  }

  @Override
  public FlyerConfig getConfig() {
    FlyerConfig config = new FlyerConfig();
    config.index = getIndex();
    config.x = getX();
    config.y = getY();
    config.z = getZ();
    config.rotation = getRotation();
    config.tilt = getTilt();
    return config;
  }
}

class WingModel extends LXModel implements Wing {
  protected int flyerIndex;
  protected boolean isRight;
  protected int index;

  /**
   * Skew about the mount point.
   * degrees the wing is down or up, range 0 to 180
   */
  protected int skew;

  protected LightSamplePoint[] lightPoints = new LightSamplePoint[NUM_LIGHT_POINTS_PER_WING];

  WingModel(int flyerIndex, boolean isRight) {
    this.flyerIndex = flyerIndex;
    this.isRight = isRight;
    this.index = getIndex();
    this.skew = 90;

    this.lightPoints[0] = new LightSamplePointModel(LightSamplePoint.LightCorners.HEAD, this.index, this.flyerIndex, COLOR, BRIGHTNESS);
    this.lightPoints[1] = new LightSamplePointModel(LightSamplePoint.LightCorners.SIDE, this.index, this.flyerIndex, COLOR, BRIGHTNESS);
    this.lightPoints[2] = new LightSamplePointModel(LightSamplePoint.LightCorners.TAIL, this.index, this.flyerIndex, COLOR, BRIGHTNESS);
  }

  @Override
  public int getFlyerIndex() {
    return this.flyerIndex;
  }

  @Override
  public boolean getIsRight() {
    return this.isRight;
  }

  @Override
  public void setSkew(int skew) {
    this.skew = skew;
  }

  @Override
  public LightSamplePoint[] getLightPoints() {
    return this.lightPoints;
  }

  @Override
  public int getSkew() {
    return this.skew;
  }
}

class LightSamplePointModel extends LXPoint implements LightSamplePoint {

  protected LightSamplePoint.LightCorners location;
  protected int index;
  protected Integer wingIndex;
  protected int bodyIndex;
  protected String color;
  protected int brightness;

  LightSamplePointModel(LightSamplePoint.LightCorners location, Integer wingIndex, int bodyIndex, String color, int brightness) {
    super();
    this.wingIndex = wingIndex;
    this.bodyIndex = bodyIndex;
    this.location = location;
    this.index = getIndex();
    this.color = color;
    this.brightness = brightness;
  }

  @Override
  public int getBodyIndex() {
    return this.bodyIndex;
  }

  @Override
  public LightCorners getLocation() {
    return this.location;
  }

  @Override
  public Integer getWingIndex() {
    return this.wingIndex;
  }

  @Override
  public void setColor(String color) {
    this.color = color;
  }
  
  @Override
  public String getColor() {
	return this.color;
  }
  

  @Override
  public void setBrightness(int brightness) {
    this.brightness = brightness;
  }
}

