import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

class IO {

  final String projectPath;

  /** CONSTRUCTOR **/
  IO(String projectPath) {
    this.projectPath = projectPath;
  }

  /***** PUBLIC FUNCTIONS *****/

  /* Loads a flyer configuration from the specified 'filename' within the project path */
  public FlyerConfig[] loadConfigFile(String filename) {
    return loadJSONFile(createPath(filename), new TypeToken<FlyerConfig[]>() {}.getType());
  }

  /* Saves the configuration 'config' to the specified 'filename' within the project path */
  public void saveConfigFile(FlyerConfig[] config, String filename) {
    saveJSONToFile(config, createPath(filename));
  }

  /* Loads a saved FLIGHT playlist from the specified 'filename' within the project path */
  public JsonArray loadSetFile(String filename) {
    return loadJSONFile(createPath(filename), JsonArray.class);
  }

  /* Loads a saved FLIGHT playlist from the specified 'file' */
  public JsonArray loadSetFile(File file) {
    return loadJSONFile(file.getPath(), JsonArray.class);
  }

  /* Saves a playlist 'array' to the specified 'filename' within the project path */
  public void saveSetFile(JsonArray array, String filename) {
    saveJSONToFile(array, createPath(filename));
  }

  /* Saves a playlist 'array' to the specified 'file' */
  public void saveSetFile(JsonArray array, File file) {
    saveJSONToFile(array, file.getPath());
  }

  /***** PUBLIC FUNCTIONS *****/

  /* Concatenate file name to the project path */
  private String createPath(String filename) {
    return projectPath + File.separator + filename;
  }

  /* Loads a JSON File from the specified 'path' and into the given type 'typeToken' */
  private static <T> T loadJSONFile(String path, Type typeToken) {
    try (Reader reader = new BufferedReader(new FileReader(path))) {
      return new Gson().fromJson(reader, typeToken);
    } catch (IOException ioe) {
      System.out.print("Error reading json file: ");
      System.out.println(ioe.getMessage());
    }
    return null;
  }

  /* Serialize and save flyerConfig list 'config' to Json */
  private static void saveJSONToFile(FlyerConfig[] config, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
    } catch (IOException ioe) {
      System.out.println("Error writing List to json file.");
    }
  }

  /* Serialize and save a playlist 'array' list to Json */
  private static void saveJSONToFile(JsonArray array, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
    } catch (IOException ioe) {
      System.out.println("Error writing JsonArray to json file.");
    }
  }
}