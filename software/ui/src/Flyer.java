interface Flyer extends GeometryConstants {
  public final static int PIXELS_PER_BODY =
    LightSamplePoint.LightCorners.BODY.numPixels;

  public final static int PIXELS_PER_FLYER =
    NUM_WINGS_PER_FLYER *
    (LightSamplePoint.LightCorners.HEAD.numPixels +
    LightSamplePoint.LightCorners.SIDE.numPixels +
    LightSamplePoint.LightCorners.TAIL.numPixels) +
    LightSamplePoint.LightCorners.BODY.numPixels;

  int getIndex();

  float getX();

  float getY();

  float getZ();

  float getRotation();

  float getTilt();

  Wing[] getWings();

  Wing getLeftWing();

  Wing getRightWing();

  LightSamplePoint getBodyLightPoint();

  FlyerConfig getConfig();
}

class FlyerConfig {
  int index;
  float x;
  float y;
  float z;
  float rotation;
  float tilt;
}