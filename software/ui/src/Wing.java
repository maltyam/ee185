interface Wing extends GeometryConstants {

  /**
   * Total number of LED pixels in wing
   */
  public final static int PIXELS_PER_WING =
    LightSamplePoint.LightCorners.HEAD.numPixels +
    LightSamplePoint.LightCorners.SIDE.numPixels +
    LightSamplePoint.LightCorners.TAIL.numPixels;

  int getFlyerIndex();
  boolean getIsRight();
  LightSamplePoint[] getLightPoints();

  default int getIndex() {
    return getFlyerIndex() * NUM_WINGS_PER_FLYER + isRightAsInt();
  }

  default int isRightAsInt() {
    return getIsRight() ? 1 : 0;
  }

  int getSkew();

  void setSkew(int skew);
}