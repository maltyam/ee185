# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores the user interface (UI) for controlling and visualizing
Python. This is an [LX Studio](https://lx.studio/)-based user interface, with 
support for playlists and dynamically controlling the piece.

