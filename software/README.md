# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores all of FLIGHT's software.

The directory structure:
  - firmware: code that runs on a FractalFlyer, providing a Python interface over a flash drive and USB tty
  - scripts: python scripts that run on the firmware
  - simulator: a graphical simulator of FLIGHT, which you can program just like the real installation (using Python)
  - ui: The user interface to controlling FLIGHT both in real-time and using predefined playlists

