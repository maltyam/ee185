# Assignment 3: Software Prototype

*Written by Philip Levis*

**Prototype Due: Tuesday, May 5, 2020 at 2:30 PM (before class)**

## Goals

When you're done with this assignment, you should
  - have an initial prototype of the sub-parts of your design working, and
  - be ready to write a clear design for your software.

## Software Design

Good software isn't cobbled together or built piecemeal. It's carefully
architected and designed. If you simply evolve your code as you realize
new complexities and problems, you end up with a brittle, clunky, and
hard-to-maintain codebase. For code that you're going to throw away, that's
fine. For example, if you plan to completely rewrite your application
every 9 months (e.g., for a web app front end), design and maintainability
is less important than velocity. But for backends, core systems, or long-lived
applications, one week of investment in design can save you tens or
hundreds of weeks of debugging and grueling refactoring down the line.

If you know exactly what you want to build, exactly what every
method and every class does, then implementing those is straightforward and
can be parallelized among many people. All you have to do is correctly and
implement all of the APIs you are responsible for, using the APIs that have
been provided to you. Software engineering can broken down into
programming assignments like those in intro CS classes.

This is the difference between tinkering or programming and *software
engineering*. A good engineer tries to resolve as many uncertainties
as possible before trying to build something. There are, of course,
always unknowns -- this is why you build prototypes. But we don't build
bridges, cars, relational databases, or robust applications by guessing,
trying, or spit and bailing wire.

It's common for software design to take *longer* than actual software
development. For example, one team I collaborate with at Google is
building secure firmware for the root of trust in computing
systems. One large concern is how code images are signed and the APIs
to manage and check these signatures, especially given that the
underlying cryptography may change significantly in the next 10-15
years. I expect it'll take a month to write and a month to test, but
it's been in design for 3 months. Building is easy and fast --
building something that will last and does its job very well requires
a lot of thought and design.

### Software Design Goals

There are many approaches to developing software, and just as many
design methodologies. It's a very complicated topic, which starts to
become a lot about tradeoffs and subtle judgements when you are in
complex projects. But [SOLID](https://www.wikipedia.org/wiki/SOLID) is
one set of high-level principles that cover a lot of the major concerns.

I suggest reading up a little on them to understand the major
concerns. The L principle isn't too important for us, as we will not
have a complex object hierarchy (many layers of subclasses). But if
you squint the right way, I think you can distill it all down to this:


> Break up your program into small, simple classes with narrow
> interfaces that make as few assumptions as possible.

### Example: Python Code Generator

Let's take an example that came up in class today: writing out Python
code to the Fractal Flyers to change their lighting and wing positions.
This functionality depends on two other abstractions:

  1. The input/output streams to send Python code to and receive output
  from Fractal Flyers.
  2. The state of the Fractal Flyers as specified by the Engine.

It also needs some way to map a Fractal Flyer to an input/output stream.

The input/output streams can be set up when the object is constructed.
Then, periodically, LXStudio will invoke an `LXOutput` to generate the
data to send to the Fractal Flyers. How all of this works isn't too
important -- all this piece of functionality wants to do is take the
state of FLIGHT and generate Python output for it.

If you dig through the LXStudio code, you'll see that you can add many
`LXOutput` instances. For SQUARED, there's a single `LXOutput`, but
each `Cluster` creates its own datagram (see
`Engine.configureExternalOutput`). We want to do something like
this. Each FractalFlyer will have an independent Python script, and we
want to generate 76 of these scripts.

It turns out the class that actually generates output is
`LXOutput`. This class is abstract -- it is not complete.  A subclass
has to fill in some functionality, such as actually generating the
data format and sending it. While LXStudio has a few existing
subclasses for popular protocols (e.g., `DDPOutput`), we need to write
a new one.  Looking through the
[documentation](https://lx.studio/api/heronarts/lx/output/), the
method we care about is `onSend`. This receives an array of color
values to write out.

It turns out LXStudio only has a model of color values, not positions
or angles! So in terms of LXStudio, this means that at some point, if
we follow this approach, wing angles will need to be put into the
global color array. THe UI won't draw them as colors, but they will
need to be there. This is kind of messy -- some values in the global
color table are colors, others are positions... yuck. This is hacky
and brittle.

Another way to do this is for an `LXOutput` to actually have a
reference to the data model, so it can directly access what the wing
positions are. You can have one `LXOutput` per Fractal Flyer. It takes
a reference to a Fractal Flyer in the data model, a
`java.io.DataInput` and a `java.io.DataOutput`. When its `onSend` is
called, it pulls the LED values and wing angles from the Fractal
Flyer, generates strings of Python code for them, and writes them to
the data output.

How the input, output, and FractalFlyer are generated or passed on
construction doesn't matter -- all this class cares is that it has
references to them, and when `onSend` is called, it can query the data
and generate the code. It's just implementing a constructor and a
single method! We could be hooked up to any `DataInput` and
`DataOutput`. We just assume the data model, and the form that Python
scrips take.

### Focus, Focus, Focus

In a project like this, where there's a pretty complicated library
we are using, it is easy to get trapped into a situation in which you
feel you have to understand everything in order to do anything. Try to
avoid this! All you need to understand is the part you're working on.
The challenge is that these parts come together into a larger design.

For example, the simulator itself (not the Python processes) just
needs to receive data from the Python scripts and update its internal
state based on those scripts. The LEDs are pretty simple: these can
just be cached. The wing positions are a little trickier, as if the
Python code tells the wings to move to angle A, there is a control
loop that controls its acceleration. We don't know exactly what this
is, so the simulator will want to factor this out into a function that
can be easily changed.  Similarly, the Python environments don't need
to worry about what the simulator does: all they need to do is turn
the Fractal Flyer Python API into some kind of data transfer to the
simulator.

## Assignment

For next Tuesday, your group should have checked in some prototype code
for your piece of the project.

  - For the simulator team, this means that a program can boot up,
  create two Python processes connected to named pipes the program
  creates , and in response to at least one of the Python API calls
  send data to the core simulator process. The core simulator process
  must be able to receive this data and update its internal state
  in real time. The key API discussion you should have is how this
  data is communicated (sockets? shared memory? RPC?). You can
  test whether this works by writing data to the named pipe (e.g.,
  with `cat`).

  - For the engine team, this means that you can boot up LX Studio,
  read in a configuration file for the physical positions of the
  Fractal Flyers, generate a data model (LXModels) for it, and send
  at least one line of Python to a named pipe that sets the LED
  values on a Flyer. You can test whether this is working by
  just setting the `DataOutput` of the code generator to be
  standard output, or a file.

For this checkpoint, the simulator and engine are not expected to work
together.

Do your work in a branch on the EE185 git repository, which you must
merge to master before class. In class, we will look over your code
together and, based on your experiences, start discussing our next
steps in software design.

## Meetings

I'd like to set up regular meetings with both teams. My schedule is
such that I either need to meet later PDT (after 8:30PM), or on
weekends between 10 and 5. Given class is on Tuesday, Saturday is
probably better than Sunday. Decide among yourselves which you would
prefer and suggest a time, I'll do my best to say yes.

Also, please feel free to email me with questions. I can be pretty
responsive with email, as I have lots of 15-20 minute windows
sprinkled through the day. I'm happy to help you figure out the
LXStudio code or give thoughts on system structure, which POSIX API to
use, or help you figure out your software design.
